package gr.mik3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import static java.time.Duration.ofSeconds;
import static org.junit.jupiter.api.Assertions.*;

class CurrentMedianTest {


    @org.junit.jupiter.api.Test
    void getMedianShortTest() {

        var shortList = new Integer[] {12,4,5,3,8,7};
        var shortListMedians= new Double[] {12.0,8.0,5.0,4.5,5.0,6.0};

        //
        // Explanation:
        // list={12} -> median = 12
        // list={12,4} -> {4,12}, median = (4+12)/2 = 8
        // list={12,4,5} -> {4,5,12}, median = 5
        // list={12,4,5,3} -> {3,4,5,12} median = (4+5)/2=4.5
        // etc.
        //
        CurrentMedian curMedian = new CurrentMedian(shortList.length);

        for (int i = 0; i < shortList.length; i++) {
            curMedian.add(shortList[i]);
            Double median = curMedian.getMedian();
            assertEquals(shortListMedians[i], median);
        }
    }

    @org.junit.jupiter.api.Test
    void getMedian_10_000_Test() throws FileNotFoundException {

        var longList = readLongListFromFile(10000);
        var longListMedians = readLongListMediansFromFile(10000);

        CurrentMedian curMedian = new CurrentMedian(longList.length);

        assertTimeout(ofSeconds(1), () -> {
            for (int i = 0; i < longList.length; i++) {
                curMedian.add(longList[i]);
                Double median = curMedian.getMedian();
                assertEquals(longListMedians[i], median);
            }
        });
    }

    @org.junit.jupiter.api.Test
    void getMedian_100_000_Test() throws FileNotFoundException {

        var longList = readLongListFromFile(100000);
        var longListMedians = readLongListMediansFromFile(100000);

        CurrentMedian curMedian = new CurrentMedian(longList.length);

        assertTimeout(ofSeconds(10), () -> {
            for (int i = 0; i < longList.length; i++) {
                curMedian.add(longList[i]);
                Double median = curMedian.getMedian();
                assertEquals(longListMedians[i], median);
            }
        });

    }



    private Integer[] readLongListFromFile(int N) throws FileNotFoundException {
        Scanner s = new Scanner(new File("medianLongList.txt"));
        ArrayList<Integer> list = new ArrayList<>();

        int i = 0;
        while (i++ < N  && s.hasNextLine())
            list.add(Integer.valueOf(s.nextLine().strip()));

        return list.toArray(new Integer[list.size()]);
    }

    private Double[] readLongListMediansFromFile(int N) throws FileNotFoundException {
        Scanner s = new Scanner(new File("medianLongListResults.txt"));
        ArrayList<Double> list = new ArrayList<>();

        int i = 0;
        while (i++ < N  && s.hasNextLine())
            list.add(Double.valueOf(s.nextLine().strip()));

        return list.toArray(new Double[list.size()]);
    }

}