package gr.mik3;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Population {
    //
    // TODO 1
    // load json file from disk
    // Fields "gender" and "birth"(integer year) are mandatory,
    // whereas "death" (integer year) is optional
    //
    // Sample data
    // {
    //	"gender": "M",
    //	"birth": 1900,
    //	"death": 1998
    // },
    //
    // Returns: total number of records in loaded array
    //
    public int loadFromFile(String fileName) throws FileNotFoundException, IOException {
        return 0;
    }

    //
    // TODO 2
    // store this person data anywhere you like - it will be used by the following get...() methods
    // If person is still alive then null is passed for "death" parameter
    //
    public void addPerson(String gender, Integer birth, Integer death) {
    }

    //
    // TODO 3
    // using the data after addPerson() method has been called (multiple times)
    // if gender == null then return entire living population
    //      else return living population for given gender (M: male, F: female)
    //
    public int getPopulationInYear(int year, String gender) {
        return 0;
    }

    //
    // TODO 4
    // get the year with the most births
    //
    public int getMostProductiveYear() {
        return 0;
    }

    //
    // TODO 5
    // get the year with the Maximum living population
    //
    public int getYearWithMaxLivingPopulation() {
        return 0;
    }

}
